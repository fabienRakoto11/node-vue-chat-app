FROM node:14.15.4
RUN mkdir -p /usr/scr/app
WORKDIR /usr/src/app/
COPY ./server/express/package.json /usr/src/app/
RUN npm install
RUN npm install -g nodemon
COPY ./server/express/ /usr/src/app/
ENTRYPOINT [ "nodemon","server.js" ]