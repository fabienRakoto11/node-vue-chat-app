const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const router = require('./router/router')
app.use(cors())
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }))

app.use('/api', router)


module.exports = app