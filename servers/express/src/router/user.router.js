const express = require('express')
const middleware = require('../middleware/auth')
const router = express.Router()
const userController = require('../controller/user.controller')


router.route('/').post(userController.createUpdate)
    .get(middleware, userController.getAll)

router.route('/login').post(userController.login)
router.route('/me').get(middleware, userController.me)

router.route('/:ID').delete(middleware, userController.deleteUser)

module.exports = router