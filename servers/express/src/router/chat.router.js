const app = require('express')
const router = app.Router()
const middleware = require('../middleware/auth')
const chatController = require('../controller/chat.controller')
router.route('/createroom').post(middleware, chatController.createRoom)
router.route('/sendmessage').post(chatController.sendMessage)
router.route('/myroom').get(chatController.mymessage)

module.exports = router