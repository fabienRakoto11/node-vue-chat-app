const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.Promise = global.Promise

const conversationSchema = new Schema({
    owner: Schema.Types.ObjectId,
    guest: Schema.Types.ObjectId,
    parentRoom: Schema.Types.ObjectId,
    message: [{
        type: Schema.Types.ObjectId,
        ref: 'message',
        default: null
    }],
    isRemoved: {
        type: Boolean,
        default: false,
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: Date
})

module.exports = mongoose.model('conversation', conversationSchema)