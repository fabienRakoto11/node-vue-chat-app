const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.Promise = global.Promise

const messageSchema = new Schema({
    sender: Schema.Types.ObjectId,
    recever: Schema.Types.ObjectId,
    body: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    isRemoved: {
        type: Boolean,
        default: false,
    },
})

module.exports = mongoose.model('message', messageSchema)