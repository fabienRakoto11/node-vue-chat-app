const bcrypt = require('bcrypt-nodejs')
const seeder = require('mongoose-seed')


seeder.connect('mmongodb://mongodb:27017/example', () => {

    seeder.loadModels([
        "../../servers/express/src/models/User",

    ])

    seeder.clearModels(['User']);

    seeder.populateModels(data, (err, done) => {
        if (err) {
            return console.error(`an error occured ${err}`);
        }
        if (done) {
            return console.log('admin seed successfully');
        }

        seeder.disconnect()
    })
});



const data = [{
    'model': 'user',
    'documents': [{
        'username': 'superadmin',
        'email': 'superadmin@urgens.io',
        'password': bcrypt.hashSync('admin')
    }]
}]