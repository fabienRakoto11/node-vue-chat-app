const Conversation = require('../models/Conversation')
const Message = require('../models/Message')
const { entity: { succes }, server: { down }, chat: { sent } } = require('../resource/constrains')
const { errorMessage, successMessage } = require('../utils')
module.exports = chatController = {
    createRoom: async(req, res) => {
        const { sender, recever } = req.body
        try {
            const senderAsMaster = new Conversation({ sender: sender, recever: recever })
            const senderAsSlave = new Conversation({ sender: recever, recever: sender })

            await senderAsMaster.save()
            await senderAsSlave.save()

            return res.status(200).json(successMessage(succes, true))
        } catch (error) {
            return res.status(200).json(errorMessage(down))
        }
    },

    sendMessage: async(req, res) => {
        const { sender, recever, chatroom, body } = req.body
        const newMessage = new Message({ sender, recever, body })
        let owner = {}

        const message = await newMessage.save()

        if (!chatroom) {

            const senderAsMaster = new Conversation({ owner: sender, guest: recever })

            await senderAsMaster.save().then(response => {
                senderAsMaster.message.push(message)
                senderAsMaster.save()
                owner = response

            })
            const senderAsSlave = new Conversation({ owner: recever, guest: sender, parentRoom: owner._id })

            await senderAsSlave.save().then(response => {
                senderAsSlave.message.push(message)
                senderAsSlave.save()
            })

            return res.status(200).json(successMessage(sent, message))

        } else {

            const homeRoom = await Conversation.findById(chatroom)
            const guestRoom = await Conversation.findOne({ parentRoom: chatroom })

            try {
                await homeRoom.message.push(message)
                await guestRoom.message.push(message)

                await homeRoom.save()
                await guestRoom.save()

                return res.status(200).json(successMessage('', message))
            } catch (error) {
                return res.status(200).json(errorMessage(down))
            }

        }
    },
    mymessage: async(req, res) => {
        const conversation = await Conversation.find({ owner: '60c93bb6119472408d6e0199' }).populate('message')
        return res.status(200).json(successMessage('', conversation))
    }
}