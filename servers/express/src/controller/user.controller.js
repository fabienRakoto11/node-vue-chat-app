const User = require('../models/User')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const { createUpdate, error_email, not_found, error_mdp, server: { down }, auth: { sign } } = require('../resource/constrains')
const { errorMessage, successMessage } = require('../utils')
const userController = {

    login: async(req, res) => {
        const { email, password, remember } = req.body

        const user = await User.findOne({ email: email })

        if (!user)
            return res.status(200).json(errorMessage(error_email))
        const isMatch = bcrypt.compareSync(password, user.password)

        if (!isMatch)
            return res.status(200).json(errorMessage(error_mdp))

        const payload = {
            user: {
                id: user.id
            }
        }

        jwt.sign(
            payload,
            'auth_admin', { expiresIn: remember === true ? "7d" : "2 days" },
            (err, token) => {
                if (err) throw err
                return res.status(200).json(successMessage(sign, { token, user }))

            }
        )

    },
    me: async(req, res) => {
        try {
            let user = await User.findById(req.user.id, '-password')
            return res.status(200).json(user)
        } catch (error) {
            return res.status(500).json(`Oops an error occured  ${error}`)
        }
    },
    getAll: async(req, res) => {
        try {
            const allUser = await User.find({ isRemoved: false }).then(response => {
                if (response)
                    return res.status(200).json(response)
            })
        } catch (error) {
            return res.status(500).json({ message: `Error occured ====> ${error}` })
        }
    },
    createUpdate: async(req, res) => {
        const data = req.body
        const user = {...data, password: bcrypt.hashSync(data.password), }

        console.log(user, "<=== user");

        try {
            if (!data.id) {
                console.log('tonga ato');
                const newUser = new User(user)
                await newUser.save(data).then(response => {
                    if (response) {
                        return res.status(201).json(successMessage(createUpdate, response))
                    }
                })
            } else {

                await User.findOneAndUpdate({ "_id": data.id }, {
                        $set: {
                            ...data,
                            updatedAt: Date.now()
                        }
                    })
                    .then(response => {
                        if (response)
                            return res.status(200).json(successMessage(createUpdate, data))
                    })
            }
        } catch (error) {
            return res.status(500).json(errorMessage(down))
        }

    },
    deleteUser: async(req, res) => {
        const id = req.params.ID
        try {
            await User.findOneAndUpdate({ _id: id }, { $set: { isRemoved: true } }).then(response => {
                if (response)
                    return res.status(200).json({ message: 'User deleted' })
            })
        } catch (error) {
            return res.status(500).json({ message: `Error occured ====> ${error}` })
        }
    }
}

module.exports = userController